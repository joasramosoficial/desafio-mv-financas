package com.mv.financas.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "CLIENTE")
public class PessoaJuridica extends Cliente {
	
	@Column(name = "CPNJ")
	private String cnpj;

	public PessoaJuridica() {
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

}
