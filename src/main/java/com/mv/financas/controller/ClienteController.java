package com.mv.financas.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.mv.financas.entity.PessoaFisica;
import com.mv.financas.repository.PessoaFisicaRepository;

@Controller
public class ClienteController {

	@Autowired
	private PessoaFisicaRepository pfr;

	@GetMapping("/lista-clientes-pfisica")
	public String showAll(Model m) {
		m.addAttribute("clientes", pfr.findAll());
		return "lista-clientes-pfisica";
	}

	@GetMapping("/show-addform-pf")
	public String showAddFormPF(Model m) {
		m.addAttribute("pf", new PessoaFisica());
		return "add-cliente-pfisica";
	}

	@PostMapping("/add-pessoa-fisica")
	public String addPessoaFisica(PessoaFisica pf, Model m) {
		pf.setData_cadastro(new Date());
		pf.setTipo_cliente(0);
		pfr.save(pf);
		return "redirect:/lista-clientes-pfisica";
	}

	@GetMapping("/show-editform-pf/{id}")
	public String showEditFormPF(@PathVariable("id") long id, Model model) {
		PessoaFisica pf = pfr.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Id do cliente é inválido: " + id));		
		model.addAttribute("pf", pf);
		return "edit-cliente-pfisica";
	}

	@PostMapping("/edit-pessoa-fisica/{id}")
	public String editPessoaFisica(@PathVariable("id") long id, PessoaFisica pf) {
		pf.setIdcliente(id);
		pfr.save(pf);
		return "redirect:/lista-clientes-pfisica";
	}

	@GetMapping("/delete-pessoa-fisica/{id}")
	public String deletePessoaFisica(@PathVariable("id") long id) {
		PessoaFisica pf = pfr.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Id do cliente é inválido" + id));
		pfr.delete(pf);
		return "redirect:/lista-clientes-pfisica";

	}
}
