O projeto foi desenvolvido usando Java com o framework Spring e Thymeleaf.
A aplicação já acessa a base de dados na oracle cloud.
Apesar disso, segue anexado o backup da própria.


**Padrões de Projeto**

**MVC**

Incorporado ao Spring Framework

**Abstract Template**

Nesse caso foi utilizado no processo de criação da classe Cliente com comportamentos padrões para cliente pessoa fisica e juridica que mais tarde seriam representadas pelas classes PessoaFisica e PessoaJuridica, ambas extendendo (herança) da classe Cliente.

**Ressalvas**

Pode-se considerar que alguns outros padrões estão incorporados ao framework que encapsula uma série de comportamentos e processos de configurações do projeto a fim de tornar a rotina de trabalho do programadores mais eficiente.
As operações de crud são organizadas essencialmente pelo framework Spring que por intermédio de anotações facilita a implementação de código.


**Procedures**

Dois pacotes foram criados.

O pkg_movimentacao e o pkg_relatorio.

**Pkg_movimentacao**

No primeiro está implementada a procedure (sp_realiza_movimentacao) que realiza a movimentação financeira seja despesa ou receita. Além disso, atualiza o saldo da conta do cliente, atualiza a receita da empresa seguindo a regra sugerida que usa as quantidade de movimentações em cada mês.

**Ressalvas**

Nem todos os cenários puderam ser testados por completo. Existe um em particular que não foi testado devidamente onde o cliente inicia sua primeira movimentação e depois de meses realiza uma nova movimentação. Nesse caso, a procedure precisa incrementar corretamente a movimentação com base no mês corrente em que ela ocorreu.

Importante ressaltar que a procedure não está totalmente coesa. O ideal é modularizar e separar algumas responsabilidades em outras procedures ou funções.


**Pkg_relatorio**
Contém sp_relatorio_saldo_todos  e sp_relatorio_clientes_todos responsáveis por gerar relatórios simples dos clientes e suas contas.

